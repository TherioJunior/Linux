# Downloading, configuring and compiling your own custom kernel
## Step by step guide to do everything related to this topic

### Download kernel source
```
wget https://cdn.kernel.org/pub/linux/kernel/v5.x/linux-5.xx.xx.tar.xz
```

# Verify source
```
wget https://cdn.kernel.org/pub/linux/kernel/v5.x/linux-5.xx.xx.tar.sign
```
```
gpg --list-packets linux-5.xx.xx.tar.sign
```
```
gpg --recv-keys <fingerprint-from-previous-step>
```
```
unxz linux-5.xx.xx.tar.xz
```
```
gpg --verify linux-5.xx.xx.tar.sign linux-5.xx.xx.tar
```

### Unpack the kernel source code
```
tar -xvf linux-5.xx.xx.tar
```
```
chown -R $(whoami):$(whoami) linux-5.xx.xx
```
```
cd linux-5.xx.xx
```
```
make mrproper
```

# Kernel configuration
### Copy default arch kernel config
```
zcat /proc/config.gz > .config
```

### Make custom config changes
From here you'll have multiple options to choose from.
If you prefer to use a rather old look to do your kernel config, you can do:
```
make menuconfig
```
If you'd prefer a newer look to do your kernel config, you can do:
```
make nconfig
```


### Compilation
This is a adaptive line of code and will always get all the threads your CPU has to offer.<br>
Something to keep in mind with this, is that your PC will hardly be use-able with this, as all your CPU threads will constantly be at 100% while you are compiling.<br><br>
If you'd still like to use your PC for some smaller programs and a webbrowser (no games), you could go with your thread count -2 so that your system has 2 threads leftover for other programs. **This will increase your compilation times though**
```
make -j $(nproc --all)
```
Another general rule of thumb would be that you have 2 GB of RAM per each thread that your CPU has. This is so that you don't run out of memory during the compilation process.<br>
While this wouldn't break anything in 99.9% of the cases, it'd be annoying since you'd have to start over with compiling again.

### Module Installation
```
make -j $(nproc --all) modules
```
```
make -j $(nproc --all) modules_install
```
These 2 commands generate a folder under /usr/lib/modules/xx.xx.xx where kernel modules, as well as DKMS modules are stored.

### DKMS kernel modules
#### Information
If you aren't quite sure if you have any extra kernel modules installed or which you have installed, consider running
```
dkms status
```
It'll give you a detailed explanation of which module is installed on which kernel with what version and architecture.

#### NVIDIA Cards (Only applies if you are using the propietary NVIDIA drivers)
Now for NVIDIA cards you'll have to uninstall the nvidia and/or nvidia-lts package used to provide your graphics drivers.<br>
We'll replace this with the official nvidia DKMS module which'll install the latest NVIDIA driver as a DKMS module on all kernels that have a module directory under /usr/lib/modules/
```
sudo pacman -S nvidia-dkms
```

The 2 commands below only apply if you updated your kernel after updating your system, in which case the DKMS installer won't automatically run for your new kernel because of the DKMS module update.<br>
**This is not needed if you compile your custom kernel with the steps mentioned above and THEN update your system. In that case your new kernels modules are already stored in /usr/lib/modules/ and the DKMS installer will automatically install to that new kernel**
```
sudo dkms install --no-depmod nvidia/515.xx.xx -k 5.xx.x
```
```
sudo depmod 5.xx.x
```
**NOTE: If you happen to be recompiling or reinstalling your custom kernel, you'll first need to uninstall every DKMS module**

#### RAZER Hardware
The 2 commands below will most likely always be needed after updating your custom kernel since the openrazer-driver doesn't get and doesn't need updates frequently.<br>
It doesn't matter if you use `openrazer-driver-git` or `openrazer-driver`. Both of these packages provide the same module (openrazer-driver), the version will just be different. 
```
sudo dkms install --no-depmod openrazer-driver/3.3.0 -k 5.xx.x
```
```
sudo depmod 5.xx.x
```

**NOTE: If you happen to be recompiling or reinstalling your custom kernel, you'll first need to uninstall every dkms module**


### Copy the kernel to /boot
```
make bzImage
```
```
sudo cp -v arch/x86/bzImage /boot/vmlinuz-linux5xx
```
**NOTE: If you happen to have a separate /boot partition you'll first need to mount that, otherwise you won't actually copy to your real /boot directory and your boot loader will most likely have problems.**

### Initial ramdisk
Only applies if you didn't directly build the needed drivers into your kernel. A inital ramdisk or initial ramfilesystem is not required given you properly configure your kernel.
```
sudo cp /etc/mkinitcpio.d/linux.preset /etc/mkinitcpio.d/linux5xx.preset
```

#### Make the following changes to the newly copied mkinitcpio preset:
```
ALL_kver="/boot/vmlinuz-linux5xx"
```
```
default_image="/boot/initramfs-linux5xx.img"
```
```
fallback_image="/boot/initramfs-linux5xx-fallback.img"
```

#### Generate initial ramdisk
```
mkinitcpio -p linux5xx
```

### Final changes
Configure your boot loader. This is very important if you want to actually use your custom kernel.<br>
More on this will follow.
