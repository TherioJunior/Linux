# Linux Kernel
#### Before doing anything I'd recommend looking into [this](https://gitlab.com/TherioJunior/Linux/-/tree/main/Kernel/Disclaimer.md). It is a file containing disclaimers and things to look out for.

#### If you'd rather look into this in form of a wiki entry rather than individual markdown files, click [here](https://gitlab.com/TherioJunior/Linux/-/wikis/home).

Things all around the kernel will be handled here.
For now, though, the only things that I'll be focusing on are custom kernels.
Possibly more to follow.

For more information on how to manually do your own custom kernel, check [this](https://gitlab.com/TherioJunior/Linux/-/tree/main/Kernel/Instructions.md) out

For more information on how to script most of your custom kernel, stay tuned.

If you need support with any of this, feel free to open an issue on this repository or look into [how to contact me.](https://gitlab.com/TherioJunior/TherioJunior/-/blob/main/README.md#contact-me-at)
