# Disclaimer



#### Why?
Why I do this and why you should maybe look into doing this?  
Now, don't get me wrong, while the linux kernel is quite bloated due to its wide support for hardware, you will still experience better performance with any linux distro than with MS/Windows 10 and 11. Especially on higher-end systems it mostly won't make a noticeable difference.  
However, I still want to go with as little bloat as possible and things like N64 controllers or tablets aren't going to be connected to my PC ever and so having the drivers for that is quite useless.


#### The general process
Before doing any changes or even thinking about doing your own custom kernel, there are multiple things to look out for, such as:
- Do not do this to appear cool in front of anybody.
- You can easily screw up your system and/or limit/disable functionality and/or features of some programs
- Look into the options you want to modify before you actually modify any of them.

#### Things to look out for
Some options may seem really useless to you judging from the name, however they may not be depending on what you use your PC for.
- General setup > Timers subsystem > High Resolution Timer Support. May not sound too useful does it? Well, if you disable this option you disable the Virtualization section of the kernel and you'll be unable to use features such as KVM.

#### What happens if you screw up and you are unable to boot?
**I am in no way responsible for you, your hardware or your operating system if you try doing this on your own. I will gladly offer support, but I am not the one to blame in case you screw up.**<br>

I am in no way responsible for what you do with your computer and operating system. This is a guide and I try to clear things up in form of disclaimers and warnings but I cannot cover every single aspect because the linux kernel is so big, which is why I actually recommend good to advanced knowledge of linux, your hardware and the programs you are using before you touch any of this.<br><br>
Both AMD and Intel have specific driver categories and options and depending on which CPU you're on you'll be able to disable the other one.<br><br>
This doesn't mean you can go around and disable options as you please though, some options maybe won't start with the CPU or GPU vendor that you are using and disabling it will brick the functionality of your system.
