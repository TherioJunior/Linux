# Linux

### Introduction

This is a repository revolving around Linux as a whole, including its kernel, Virtual Machine Management and Hypervisors, Shells and all you can think of. It largely contains personal notes, optimisation techniques and configuration files. You can use all of these to your hearts content as you please.

### Linux Kernel

For information about the Linux kernel and how to compile your own version based on your own configuration file, head over to the [Kernel](./Kernel/) directory. It contains build instructions and disclaimers for compiling your version of the Linux kernel.

### Desktop Environments

For information about Linux Desktop Environments, head over to the [Desktop Environment](./Desktop Environment/) directory. It contains information about various things related to different Desktop Environments, and will likely continue to grow.

### QEMU/KVM managed through Libvirt

For information about [QEMU/KVM](https://qemu.org) managed through [Libvirt](https://libvirt.org) and the [Virtual Machine Manager](https://virt-manager.org) head over to the [QEMU/KVM](./QEMU-KVM/) directory. It contains information about installing and setting up QEMU/KVM, as well as Libvirt and the Virtual Machine Manager. Additionally, it'll also contain information about optimisation techniques, things to look out for when using QEMU/KVM.

### Additional notes

- When using the [fish shell](https://fishshell.com) configuration files inside this repository, you might need additional packages, as that configuration file contains aliases that control other packages. I'll include notes and mentions of required packages.

- I am not to blame when something happens to your system. I will always try to include instructions on how to disable the changes that were made, but I am not liable for issues you experience when using any of the content featured here :) That said, these things work for me, so there's a good chance they'll work for you.