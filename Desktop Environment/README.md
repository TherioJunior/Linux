# Desktop Environments

## Introduction
This is a collection of commands somewhat commonly required for your DE (Desktop Environments). It may also contain X11 or Wayland information in the future.

## Refreshing your Desktop Icon cache without restarting your entire shell/logging out and in
I only confirmed this to be working with KDE, from what I've read you have to log out and back in when using GNOME, and I don't know about any other DE compatibility with these commands.

```shell
sudo kbuildsycoca5 --noincremental && sudo update-desktop-database
```
These can be executed with or without sudo, it just depends which desktop icon caches or .desktop files you want to refresh. For those inside your user home folder (.local/share/applications for example), executing those commands without sudo will work, but when working with the global desktop file entries, you'll have to use sudo.
Also note, that kbuildsycoca5 is likely KDE specific, and on other desktop environments you'd only execute `sudo update-desktop-database`.

