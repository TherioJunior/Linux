# QEMU/KVM

## Introduction

Here I will cover everything around QEMU/KVM. This document will be quite large from my estimates when starting to write this, but it'll provide tutorials on how to do everything, as well as technical notes, reasons and considerations.

In the case of guest optimisations, I will only provide the raw XML configuration and instructions on where to put them. This repository will not cover the raw QEMU/KVM Hypervisor which is managed through the CLI.

## Table of contents

- [Terms used](#terms-used)
  
- [Installation](#installation)
  
- [CPU Optimisation](#cpu-optimisation)
  
- [GPU Optimisation](#gpu-optimisation)
  
- [RAM Optimisation](#ram-optimisation)
  
- [Kernel Optimisation](#kernel-optimisations)
  
- [Bootloader Configuration](#bootloader-configuration)
  

## Terms used
- Host: This is your main PC, on which you run VMs

- VM: Virtual Machine

- VMM: Virtual Machine Manager

- VMV: Virtual Machine Viewer

- CLI: Commandline Interface

- vCPUs: Virtual CPUs. This is the "virtualised" CPU that the VM is assigned.

- pCPU: This is the CPU in your host

<br>

## Installation

TODO: Include full QEMU/KVM, Libvirt, Virtual Machine Manager, UEFI & UEFI SecureBoot and Trusted Platform Module (TPM) installation tutorials.

<br>

## CPU Optimisation

#### XML-based CPU configuration enhancements

For the best guest performance, you will more than likely manually have to edit the XML configuration inside VMM, but don't worry, I'll guide you through it. First, begin by opening the `Details` tab inside the VMV. Then, open the `XML` tab at the top, it should be one of two options, and relatively easy to find.

Then, you'll want to change your `<cpu ......>` configuration to the following:

```xml
<cpu mode="passthrough" check="none" migratable="off">
    <topology sockets="1" cores="x" threads="2"/>
</cpu>
```

There are a lot more things to add inside the CPU tag of the configuration, but we'll dive into that later on in this repository.

#### vCPU Pinning

In QEMU you have the ability to pin the guests' vCPU cores to specific pCPU cores of your host. This is for example beneficial if you are doing a gaming VM and you have a *X3D CPU, you could assign the VM specifically the 3D VCache cores, which'd provide you with additional gaming performance.

Another use case for this is, if you have multiple VMs, that their workloads don't limit each other, by pinning VM one to cores 1-4, and VM two to cores 5-8.

You will want to place the XML in the codeblock below, right under the following XML snippet, though it doesn't really matter I think, considering VMM automatically formats everything for you.

```xml
<cputune>
    <vcpupin vcpu="0" cpuset="7"/>
    <vcpupin vcpu="1" cpuset="6"/>
    <vcpupin vcpu="2" cpuset="5"/>
    <vcpupin vcpu="3" cpuset="4"/>
    <vcpupin vcpu="4" cpuset="3"/>
    <vcpupin vcpu="5" cpuset="2"/>
</cputune>
```
Place the above XML right below the following
```xml
<vcpu placement="static">6</vcpu>
````
Each `vcpu="x"` statement in this is responsible for one vCPU thread each (*2 your pCPU core count), and each `cpuset="x"` statement is similarly responsible for one pCPU thread each.

The above `<cputune>` statement, is designed for a `3 cores/6 threads` VM, and will effectively pin the vCPU cores and threads in the reverse order of the pCPU cores and threads. This is useful to for example keep the vCPU core 0 off your pCPU core 0, which is good for performance. Why you might ask? Well, modern operating systems do a lot of kernel work, timings and other things on core 0. On a traditional PC setup this doesn't matter a lot, as you're only ever running 1 kernel at a time. This is somewhat problematic though if you are running various VMs, as all those kernels of all your VMs run on core 0.

To give a practical example of this, say you're running 3 VMs, pCPU core 0 would then have to handle 4 different kernel operations.

You can also pin multiple pCPU cores or threads to one single vCPU core or thread, effectively doubling vCPU performance on that one core or thread.

#### Manual Topology

By default, VMM will configure your VM to use an automatic topology, which most of the time specifies each vCPU core as a CPU socket for the VM. While this works, it is not the best solution performance-wise.

Instead, you will want to go to the `CPUs` section inside VMM, expand the `Topology` section, and enable `Manually set CPU Topology`. Then, you will set the `Sockets` to `1`, `Cores` to half the cores you want the VM to be able to use, and `Threads` to `2`. This will "emulate" a pCPU configuration for the guest, which can help with performance, and in the case of gaming VMs, it'll help you stay undetected from anti-cheats.

#### Cache modes

By default, a emulated CPU cache will be used by QEMU. This works, but when you do CPU intensive tasks such as compilation of applications or gaming, this can hurt your guest performance.

That said, the steps to enable this can be somewhat advanced for entry-level users, as there is no way to do this without directly modifying the VMs XML configuration inside VMM.

You will want to find the `<cpu ......>` section of your VMs configuration. Most of the time, this is not too far down. Then you want to append the following thing:

```xml
<cpu mode="passthrough" check="none" migratable="off">
    <topology sockets="1" cores="x" threads="2"/>
    <cache mode="passthrough"/>
</cpu>
```

This will provide better and more efficient access to your pCPUs cache, thus improving overall usability and guest performance.

#### Feature Policies
QEMU allows you to have `<feature policy="require/disable" name="x"/>` statements, which sometimes modify guest behaviour, and other times enable/disable certain features of your pCPU.

To view all your available CPU features, execute the following command (no root needed):
```bash
grep -m 2 -w flags /proc/cpuinfo
```

Below you'll find a complete list of AMD and Intel specific CPU flags, as well as a section for general flags, which are available on both CPUs. Some of these have notes added to them, as they can negatively and positively affect guest performance.

##### Common to Both Intel and AMD CPUs:

1. `hypervisor`: Disable the Hypervisor policy. Very negatively affects AMD performance, but protects you from tripping Type2 Hypervisor checks.
2. `acpi`: Advanced Configuration and Power Interface.
3. `apic`: Advanced Programmable Interrupt Controller.
4. `mmx`: MultiMedia eXtensions.
5. `fxsr`: FXSAVE and FXRSTOR instructions.
6. `sse`: Streaming SIMD Extensions.
7. `sse2`: Streaming SIMD Extensions 2.
8. `ss`: Self-snoop.
9. `ht`: Hyper-threading.
10. `tm`: Thermal Monitor.
11. `pae`: Physical Address Extension.
12. `pat`: Page Attribute Table.
13. `pclmulqdq`: PCLMULQDQ instruction.
14. `cx16`: CMPXCHG16B instruction.
15. `pdpe1gb`: One GB pages.
16. `sse4.1`: Streaming SIMD Extensions 4.1.
17. `sse4.2`: Streaming SIMD Extensions 4.2.
18. `avx`: Advanced Vector Extensions.
19. `fma`: Fused Multiply Add.
20. `syscall`: Fast system calls.
21. `nx`: No-execute bit.
22. `constant_tsc`: Constant Time Stamp Counter.
23. `nopl`: NOP instruction.
24. `x2apic`: x2 Advanced Programmable Interrupt Controller.
25. `popcnt`: POPCNT instruction.

##### Intel CPUs:

1. `vmx`: Enables Intel Virtualization Technology (Intel VT).
2. `smx`: Enables Safer Mode Extensions (part of Intel VT).
3. `pcid`: Process-context identifiers.
4. `pdcm`: Performance and Debug Capability MSR.
5. `xtpr`: xTPR Update Control.
6. `dca`: Direct Cache Access.
7. `tsc-deadline`: TSC deadline.
8. `aes`: Advanced Encryption Standard instructions.
9. `pclmulqdq`: Carry-less Multiplication instructions.
10. `f16c`: 16-bit Floating-Point conversion instructions.
11. `rdrand`: Read Random Number instructions.
12. `fsgsbase`: Access FS and GS base instructions.
13. `rdseed`: Read Seed Random Number instructions.
14. `smap`: Supervisor Mode Access Prevention.
15. `mpx`: Memory Protection Extensions.
16. `clflushopt`: Optimized Cache Line Flush Instruction.
17. `clwb`: Cache Line Write Back.
18. `pcommit`: Persistent Commit Instruction.
19. `sgx`: Software Guard Extensions.

##### AMD CPUs:

1. `svm`: Enables AMD-V (AMD Virtualization).
2. `abm`: Advanced Bit Manipulation (LZCNT instruction support).
3. `sse4a`: Streaming SIMD Extensions 4a.
4. `misalignsse`: Misaligned SSE mode.
5. `3dnowprefetch`: 3DNow! Prefetch instructions.
6. `osvw`: OS Visible Workaround.
7. `ibs`: Instruction Based Sampling.
8. `skinit`: SKINIT/STGI instructions.
9. `wdt`: Watchdog timer.
10. `tce`: Translation Cache Extension.
11. `topoext`: Topology Extensions.
12. `perfctr-core`: Core performance counter extensions.
13. `perfctr-nb`: NorthBridge performance counter extensions.
14. `bpext`: Data breakpoint extension.
15. `perftsc`: Performance Timestamp Counter.
16. `amd-stibp`: AMD Single Thread Indirect Branch Predictors.
17. `amd-ssbd`: AMD Speculative Store Bypass Disable.

Which of these policies you use is up to you, they somewhat depend on what you use your VMs for, and also which CPU you have. I haven't tested all of these myself and what purpose they have, and I am also on an Intel system, so my mentions of the `hypervisor` policy negatively affecting performance is based off of what a friend told me when he tried setting up a gaming VM.

I will however tell you a few basic policies you can use to improve guest performance.
If you're on Intel, you'll want to at least use these policies:
```xml
<feature policy="require" name="ht"/>
<feature policy="require" name="dca"/>
```
If you're on AMD, you'll want to at least use these policies:
```xml
<feature policy="require" name="ht"/>
<feature policy="require" name="topoext"/>
```
Any other policies are up to you and depend on what you use your VMs for.

<br>

## GPU Optimisation
Afaik there isn't a whole lot we can do here. The main way of having good GPU performance on QEMU would be to passthrough a physical PCIe device such as a GPU to the VM. Some users don't like this, which is fine, but you don't have a lot of other options to go with.

##### PCIe Device Passthrough
For information about these things please refer to the [ArchWiki section about it](https://wiki.archlinux.org/title/PCI_passthrough_via_OVMF), as well as [RisingPrisms GitLab Repository about it](https://gitlab.com/risingprismtv/single-gpu-passthrough/-/wikis/home), as well as probably one of my favourite all-in-one [YouTube tutorial about this](https://piped.adminforge.de/watch?v=53ZO2pKS4i0). Note that the GitLab Repo and the YouTube video both focus on Single GPU Passthrough, since I don't expect most people to just have 2 GPUs.

##### VirtIO GPU
So there's a VirtIO GPU Option which supposedly supports 3D Acceleration of guests, though I have never gotten it to work, so I cannot talk too much about it.

##### Intel iGPU Splitting
There's also the ability to split Intel iGPUs into multiple separate iGPUs, but the payoff was not worth it in my opinion, so I will not cover it here.

<br>

## RAM Optimisation
There's actually a lot we can do here! I will also include a sub-section here, on how to improve startup times of VMs, as after some kernel update in the 5.* releases a technology which was used by QEMU to quickly allocate large amounts of RAM was disabled by default, making startup times for VMs with large amounts of RAM allocated to them quite long.

##### Transparent Hugepages (Host Adjustment)
This is the first technology we will focus on. So by default Linux virtual memory pages (if you want to find out what those are, skip to the next step and come back after) are 4 KiB. When running a lot of VMs or when having high RAM usages in general, this can be a lot for your CPU to handle, depending on your CPU model. Transparent Hugepages aim to mitigate this problem, or at least make it easier for your CPU to handle. It increases the size of virtual memory pages to 2 MiB.

Transparent Hugepages come with two main settings: `enabled` and `defrag`.
To check both of those, check out the two commands below:
```Shell
cat /sys/kernel/mm/transparent_hugepage/enabled
```
and
```Shell
cat /sys/kernel/mm/transparent_hugepage/defrag
```
Your output will be composed of a few words, in which the word inside block brackets `[]` is the currently active value. All Linux systems I know of will start off with the `madvise` setting, which is a Linux syscall, which applications that benefit from 2 MiB memory pages can call to make their memory pages 2 MiB of size. To manually enable Transparent Hugepages and their defragmentation feature you can execute the two commands below:
```Shell
echo "always" > /sys/kernel/mm/transparent_hugepage/enabled
```
and
```Shell
echo "always" > /sys/kernel/mm/transparent_hugepage/defrag
```
Something to look out for when enabling this, is that you will need to be the `root` user to do so. Using `sudo` is not enough when I tested it, you have to be the `root` user specifically.

Something to look out for when using this, is that some applications may be negatively affected by this, and that this introduces some additional CPU overhead.

##### KSM (Kernel Same-page Merging) (Host Adjustment)
This is the second technology we will focus on. The technical aspects come down to the following:
- Modern PCs have a lot of RAM, so physical memory is being split up into virtual memory pages, to make managing it easier.
- KSM essentially scans a amount of memory pages you can configure in a timeframe you can also configure.
- If KSM finds multiple memory pages which are identical, it deduplicates them and from what I understand just includes references to that deduplicated page for multiple applications that use it

Usually on normal setups KSM would not be too useful, but for multiple VMs which run the same OS or even kernel, overlap of memory pages can very well happen. Enabling this will reduce your overall RAM usage.

To set up KSM you will need to create a `systemd` service at `/etc/systemd/system` named `ksm.service`, or another name of your choice.
```
[Unit]  
Description=Kernel Same-Page Merging (KSM) Configuration Service  
After=network.target  
  
[Service]  
Type=oneshot  
ExecStart=/bin/bash -c "echo 1 > /sys/kernel/mm/ksm/run; \  
                        echo 10000 > /sys/kernel/mm/ksm/pages_to_scan; \  
                        echo 2000 > /sys/kernel/mm/ksm/sleep_millisecs"  
ExecStop=/bin/bash -c "echo 0 > /sys/kernel/mm/ksm/run"  
RemainAfterExit=yes  
  
[Install]  
WantedBy=multi-user.target
```
The `10000` here stands for the pages to scan during each run, and `2000` is the delay in milliseconds to sleep between each run. Adjust this to your hearts content and with whatever works best for you. To start this and enable it on boot, execute the following command:
```Shell
sudo systemctl enable --now ksm.service
```
This does not require a reboot, and stopping the service will automatically disable KSM again.

In case you changed the service configuration and now have shorter durations between scans or more pages to scan, you'll want to execute the following set of commands
```Shell
sudo systemctl daemon-reload
```
Which will reload the services present on your disk, followed by:
```Shell
sudo systemctl restart ksm.service
```
Something to look out for when using KSM, is that it will introduce additional CPU overhead, since it has to scan and compare the virtual memory pages present in your system.

##### Memballoon Devices (Guest Adjustment)
So `Memballoon` devices are essentially what you'd imagine by its name. They are balloons containing memory, and similar to balloons they can inflate and deflate. If a guest does not have a `Memballoon` device attached, it will allocate all the RAM given to it on boot.

A disclaimer before we go into this, is that you may need to make adjustments via scripts and systemd services inside the guest to make `Memballoon` devices most efficient, but more on that later.

Another disclaimer is that `Memballoon` devices are generally not recommended on Windows guests. They work more than well on Linux guests though :)

To make it possible for the guest to have a `Memballoon` device, you may need to edit its XML configuration, but I think most guests created using VMM have a `Memballoon` device attached to them by default.

Now to the adjustments inside the guest. Linux by nature caches quite aggressively, and free RAM is often times used as filesystem or operating system operation cache. For VMs below 8 GB of RAM, you more than likely won't see a difference with a `Memballoon` device attached, as for example a guest with 3 GB RAM usage, will allocate the additional 3 GB RAM as cache. You can manually `sync` filesystem operations though, and drop caches safely, without majorly hurting guest performance.

This is a script called `clear-caches.sh`, which will run indefinitely every 60 seconds in combination with a systemd service called `clear-caches.service` located under `/etc/systemd/system/`. I like to keep scripts inside a `Scripts` directory inside a normal users `$HOME` directory, this will be reflected inside the example `systemd` service.

The service:
```
[Unit]
Description=Clear Cache Service

[Service]
ExecStart=/home/<YOUR USERNAME>/Scripts/clear-caches.sh # Works on normal distributions such as Debian 11 or Manjaro
#ExecStart=/bin/bash /var/home/<YOUR USERNAME>/Scripts/clear-caches.sh # For immutable filesystems such as Fedora Silverblue or Kinoite
User=root

[Install]
WantedBy=multi-user.target
```
Disclaimers for specific system types are included via comments inside the service file. The script has the following code:
```
#!/bin/bash
# Clear cache script

while true; do
    sync
    echo 3 > /proc/sys/vm/drop_caches
    sleep 60
done
```
Where `sync` syncs ongoing filesystem operations and `echo 3 > /proc/sys/vm/drop_caches` drops filesystem and inode caches.

The above service file and script can reduce the RAM usage of a `6 GB` Linux guest from `6 GB` to `4 GB`.

##### Improving start up performance
TODO: Include tutorial for preempt=voluntarely kernel bootloader commandline

<br>

## Kernel Optimisations

<br>

## Bootloader Configuration